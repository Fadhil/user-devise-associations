class SessionsController < ApplicationController
    def new
        @user = User.new
    end
    
    def create
        @user = User.find_by_email(params[:session][:email])
        
        if @user
            if @user.authenticate(params[:session][:password])
                session[:user_id] = @user.id
                redirect_to user_path(id: @user.id), notice: 'Welcome Back!'
            else
                redirect_to sign_in_path, alert: 'Invalid username/password' 
            end
        else
           redirect_to sign_in_path, alert: 'Invalid username/password' 
        end
    end
    
    def destroy
       session.delete(:user_id)
       redirect_to sign_in_path, notice: 'Thanks for visiting! Come back again!'
    end
end
