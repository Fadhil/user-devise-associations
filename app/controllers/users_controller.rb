class UsersController < ApplicationController
  def index
    @users = User.all
  end

  def show
    @user = User.find(params[:id])
    
    respond_to do |format|
      format.html
      format.json { render json: @user }
    end
  end
  
  def new
    @user = User.new
  end
  
  def edit
    #@user = User
  end
   
  def create
    @user = User.new(valid_user_params)
    
    if @user.save
      redirect_to user_path(id: @user.id), notice: 'Successfully Signed Up'
    else
      render :new
    end
  end
  
  def valid_user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end

