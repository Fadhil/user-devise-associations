class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
    validates :name, presence: true
    validates :email, presence: true, format: { with: /\A\S+@\S+\.\S+\z/, message: 'is invalid format' }
   # has_secure_password
   
   has_many :time_zones
end
