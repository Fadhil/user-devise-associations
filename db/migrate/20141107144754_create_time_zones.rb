class CreateTimeZones < ActiveRecord::Migration
  def change
    create_table :time_zones do |t|
      t.string :region
      t.integer :offset

      t.timestamps
    end
  end
end
